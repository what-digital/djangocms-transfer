Installation
=============
- Add the lib reference to your requirements.txt
- Add djangocms_transfer to your INSTALLED_APPS

Settings
=============
Add it to your settings.py if you want to change configurations::

    TRANSFER_CONFIG = {
        'export_files_dir_name': 'page_data_exported',
        'json_file_name': 'data.json',
        'zip_file_name': 'media.zip',
    }

Using It
=============

Export the page the data::

    $ python manage.py export_page_tree --page-id=<page-id>

Import the page the data::

    $ python manage.py import_page_tree --json_file=<path-to-the-data.json> --zip_file=<path-to-the-media.zip> --domain=<target-web-site-name>
