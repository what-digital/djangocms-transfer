from cms.models import Page
from cms.models import Title
from cms.models import TreeNode
from cms.utils.page import get_available_slug
from django.template.defaultfilters import slugify

from djangocms_transfer.importer.plugin import import_plugins_to_page


def load_page_tree(data, site, parent_page=None):
    created_pages = list()
    page = create_new_page(data, site, parent_page)
    load_page(data=data, page=page)
    created_pages.append(page)
    if data.get('child_pages'):
        for page_languages in data['child_pages']:
            pages_list = load_page_tree(
                data=page_languages,
                site=site,
                parent_page=page,
            )
            created_pages.extend(pages_list)
    return created_pages


def create_new_page(page_data, site, parent_page=None):
    if parent_page:
        new_node = TreeNode(site=site)
        parent_page.node.add_child(instance=new_node)
    else:
        new_node = TreeNode.add_root(site=site)

    new_page = Page.objects.create(node=new_node)

    page_attrs_data = page_data['page_attrs']

    new_page.application_namespace = page_attrs_data['application_namespace']
    new_page.application_urls = page_attrs_data['application_urls']
    new_page.languages = page_attrs_data['languages']
    new_page.login_required = page_attrs_data['login_required']
    new_page.navigation_extenders = page_attrs_data['navigation_extenders']
    new_page.reverse_id = page_attrs_data['reverse_id']
    new_page.template = page_attrs_data['template']
    new_page.save()

    return new_page


def load_page(data, page):

    for title_data in data['titles']:
        load_titles(title_data, page)

    # page.publish(language=language)


def load_titles(data, page):
    placeholders_data_list = data.pop('placeholders')
    title = Title.objects.create(page=page, **data)
    base = page.get_path_for_slug(slugify(data['title']), data['language'])
    slug = get_available_slug(page.node.site, base, data['language'])
    path = page.get_path_for_slug(slug, data['language'])

    title.slug = slug
    title.path = path

    title.save()
    page.title_cache[title.language] = title

    import_plugins_to_page(
        placeholders=placeholders_data_list,
        page=page,
        language=title.language
    )
