import importlib
import zipfile

import os
from django.conf import settings

from djangocms_transfer.importer.folders import restore_folders_structure


def restore_media_data(plugin, files):
    for field, data in files.items():
        filer_file_data = data['filer_file_data']
        file_kwargs = dict(
            original_filename=filer_file_data['original_filename'],
            file=filer_file_data['file']
        )
        height = filer_file_data.get('_height')
        if height:
            file_kwargs['_height'] = height

        width = filer_file_data.get('_width')
        if width:
            file_kwargs['_width'] = width

        folders_data = data['folders']
        folders_data_keys = sorted(folders_data.keys())

        last_created_folder = None
        for folder_level in folders_data_keys:
            last_created_folder = restore_folders_structure(
                folder_data=folders_data[folder_level],
                parent_folder=last_created_folder
            )
        module = importlib.import_module(filer_file_data['module'])
        klass = getattr(module, filer_file_data['class_name'])
        files = klass.objects.filter(**file_kwargs)

        if last_created_folder.pk in files.values_list('folder', flat=True):
            media_file_instance = files.filter(folder=last_created_folder).first()
        else:
            media_file_instance = klass.objects.create(**file_kwargs)
            media_file_instance.folder = last_created_folder
            media_file_instance.save()

        setattr(plugin, field, media_file_instance)
        plugin.save()


def unpack_media_files(path):
    with zipfile.ZipFile(path, 'r') as zip_ref:
        zip_ref.extractall(os.path.join(settings.MEDIA_ROOT))
