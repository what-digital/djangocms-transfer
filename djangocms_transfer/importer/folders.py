from filer.models import Folder


def restore_folders_structure(folder_data, parent_folder=None):
    if parent_folder:
        folders = parent_folder.children.filter(**folder_data)
        if folders.exists():
            return folders.first()
        else:
            return Folder.objects.create(
                parent=parent_folder, **folder_data)

    folders = Folder.objects.filter(**folder_data)
    if folders.exists():
        return folders.first()

    return Folder.objects.create(**folder_data)
