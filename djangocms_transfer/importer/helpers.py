import json

import datetime
import re

from djangocms_transfer.importer.datastructures import ArchivedPlaceholder
from djangocms_transfer.importer.datastructures import ArchivedPlugin


def get_parsed_data(json_file_path):
    with open(json_file_path, 'r') as json_file:
        raw = json_file.read().decode('utf-8')
        return json.loads(raw, object_hook=object_version_data_hook)


def object_version_data_hook(data):
    if not data:
        return data

    data = datetime_decoder(data)
    if 'plugins' in data:
        return ArchivedPlaceholder(
            slot=data['slot'],
            plugins=data['plugins'],
        )
    if 'plugin_type' in data:
        return ArchivedPlugin(**data)

    return data


def datetime_decoder(dict_var):
    for key, value in dict_var.items():
        if type(value) is unicode and re.match('^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$', value):
            dict_var[key] = datetime.datetime.strptime(value, "%Y-%m-%dT%H:%M:%SZ")

    return dict_var


def get_page_urls(domain, pages_list):
    urls_list = list()
    for page in pages_list:
        languages = page.title_set.values_list('language', flat=True)
        for language in languages:
            url = page.get_absolute_url(language=language)
            urls_list.append(domain + url)

    return '\n'.join(urls_list)
