import functools
import json

import os
from cms.models import Title
from django.conf import settings
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder

from djangocms_transfer.config import TRANSFER_CONFIG

dump_json = functools.partial(json.dump, cls=DjangoJSONEncoder)


def get_clear_data(obj):
    field_names = obj.__dict__.keys()
    fields = [
        field_name for field_name in field_names if
        not field_name.startswith('_') or
        not field_name == 'id' or
        not field_name == 'pk' or
        '_id' not in field_name
    ]

    _plugin_data = serializers.serialize('python', (obj,), fields=fields)[0]
    data = _plugin_data['fields']

    return data


def get_files_paths():
    path = '{root_dir}/'.format(
        root_dir=TRANSFER_CONFIG['export_files_dir_name'],
    )

    if not os.path.exists(path):
        os.makedirs(path)

    return dict(
        json_file=os.path.join(
            settings.BASE_DIR, path, TRANSFER_CONFIG['json_file_name']),
        zip_file=os.path.join(
            settings.BASE_DIR, path, TRANSFER_CONFIG['zip_file_name']),
    )
