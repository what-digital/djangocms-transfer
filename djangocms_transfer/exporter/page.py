from djangocms_transfer.exporter.helpers import get_clear_data
from djangocms_transfer.exporter.placeholder import get_placeholder_export_data


def get_page_tree_data(page):
    collected_data = collect_page_data(page)

    child_pages = page.get_child_pages()
    if child_pages.exists():
        child_pages_data_list = list()
        for child_page in child_pages:
            child_page_data = get_page_tree_data(page=child_page)
            child_pages_data_list.append(child_page_data)
        collected_data['child_pages'] = child_pages_data_list

    return collected_data


def collect_page_data(page):
    page_data = dict()
    page_data['page_attrs'] = get_page_attrs(page)
    page_data['titles'] = get_page_titles(page)

    return page_data


def get_page_attrs(page):
    return get_clear_data(page)


def get_page_titles(page):
    titles_data_list = list()
    titles = page.title_set.all()

    for title in titles:
        title_data = get_clear_data(title)
        title_data['placeholders'] = get_page_placeholders_data(page, title.language)
        titles_data_list.append(title_data)

    return titles_data_list


def get_page_placeholders_data(page, language):
    placeholders_list = list()
    placeholders = page.rescan_placeholders().values()

    for placeholder in list(placeholders):
        placeholders_list.append(dict(
            slot=placeholder.slot,
            plugins=get_placeholder_export_data(placeholder, language)
        ))

    return placeholders_list
