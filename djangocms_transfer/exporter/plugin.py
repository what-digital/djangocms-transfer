from cms.utils.plugins import get_plugin_class
from django.core import serializers

from djangocms_transfer.exporter.media_data import save_plugins_images_data


def get_plugin_data(plugin):
    plugin_fields = get_plugin_fields(plugin.plugin_type)

    fields_names = [field.name for field in plugin_fields]
    _plugin_data = serializers.serialize('python', (plugin,), fields=fields_names)[0]
    data = _plugin_data['fields']

    plugin_data = {
        'pk': plugin.pk,
        'creation_date': plugin.creation_date,
        'position': plugin.position,
        'plugin_type': plugin.plugin_type,
        'parent_id': plugin.parent_id,
        'data': data,
        'files': save_plugins_images_data(plugin)
    }

    return plugin_data


def get_plugin_fields(plugin_type):
    klass = get_plugin_class(plugin_type)
    opts = klass.model._meta.concrete_model._meta
    fields = opts.local_fields + opts.local_many_to_many
    return fields



