import itertools
from cms.utils.plugins import get_bound_plugins

from djangocms_transfer.exporter.plugin import get_plugin_data


def get_placeholder_export_data(placeholder, language):
    get_data = get_plugin_data
    plugins = placeholder.get_plugins(language)
    # The following results in two queries;
    # First all the root plugins are fetched, then all child plugins.
    # This is needed to account for plugin path corruptions.
    plugins = itertools.chain(
        plugins.filter(depth=1).order_by('position'),
        plugins.filter(depth__gt=1).order_by('path'),
    )
    return [get_data(plugin) for plugin in get_bound_plugins(list(plugins))]
