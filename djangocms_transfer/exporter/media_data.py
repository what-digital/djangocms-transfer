import zipfile

from filer.fields.file import FilerFileField

from djangocms_transfer.exporter.helpers import get_clear_data
from djangocms_transfer.exporter.helpers import get_files_paths


def save_plugins_images_data(plugin):
    plugin_images_data = {}
    paths_dict = get_files_paths()
    for field in plugin._meta.fields:
        if FilerFileField in field.__class__.__mro__:
            image_field_value = getattr(plugin, field.name)
            if not image_field_value:
                continue

            save_media_files(
                image_field_value=image_field_value,
                zip_file=paths_dict['zip_file']
            )

            plugin_images_data[field.name] = get_media_object_data(image_field_value)

    return plugin_images_data


def save_media_files(image_field_value, zip_file):
    with zipfile.ZipFile(zip_file, mode="a") as zip_file:
        image_field_value.file.open()
        media_file = image_field_value.file
        zip_file.writestr(media_file.name, media_file.read())
        image_field_value.file.close()


def get_media_object_data(field_value):
    folders_data = dict()
    family = field_value.folder.get_family()
    for folder in family:
        folders_data[folder.level] = collect_folder_data(folder=folder)

    filer_file_data = dict(
        original_filename=field_value.original_filename,
        module=field_value.__module__,
        class_name=field_value.__class__.__name__,
        file=field_value.file.name
    )
    height = getattr(field_value, '_height', None)
    if height:
        filer_file_data['_height'] = height

    width = getattr(field_value, '_width', None)
    if width:
        filer_file_data['_width'] = width

    return dict(
        filer_file_data=filer_file_data,
        folders=folders_data
    )


def collect_folder_data(folder):
    data = get_clear_data(folder)

    data.pop('uploaded_at')
    data.pop('created_at')
    data.pop('modified_at')

    return data
