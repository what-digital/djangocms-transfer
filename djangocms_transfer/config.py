from django.conf import settings

TRANSFER_CONFIG = getattr(
    settings,
    'TRANSFER_CONFIG',
    dict(
        export_files_dir_name='page_data_exported',
        json_file_name='data.json',
        zip_file_name='media.zip',
    )
)
