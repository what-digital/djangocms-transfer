import os
from cms.models import Page

from django.core.management.base import BaseCommand

from djangocms_transfer.exporter.helpers import dump_json
from djangocms_transfer.exporter.helpers import get_files_paths
from djangocms_transfer.exporter.page import get_page_tree_data


class Command(BaseCommand):
    help = 'Exports the specified django-cms page'

    def add_arguments(self, parser):
        parser.add_argument("--page-id", type=int, required=True)

    def handle(self, *args, **options):
        page_id = options.get('page_id')
        page = Page.objects.get(pk=page_id)

        paths_dict = get_files_paths()

        if os.path.exists(paths_dict['zip_file']):
            os.remove(paths_dict['zip_file'])

        data = get_page_tree_data(page=page)

        with open(paths_dict['json_file'], 'w') as json_file:
            dump_json(data, json_file)

        self.stdout.write(self.style.SUCCESS(
            'Successfully saved data to: \n{json_file} \n{zip_file}'.format(
                json_file=paths_dict['json_file'],
                zip_file=paths_dict['zip_file']
            )
        ))
