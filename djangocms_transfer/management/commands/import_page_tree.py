from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand

from djangocms_transfer.importer.helpers import get_page_urls
from djangocms_transfer.importer.helpers import get_parsed_data
from djangocms_transfer.importer.media_data import unpack_media_files
from djangocms_transfer.importer.page import load_page_tree


class Command(BaseCommand):
    help = 'Imports the django-cms page data'

    def add_arguments(self, parser):
        parser.add_argument("--json_file", type=str, required=True)
        parser.add_argument("--zip_file", type=str, required=True)
        parser.add_argument("--domain", type=str, required=True)

    def handle(self, *args, **options):
        json_file_path = options.get('json_file')
        zip_file_path = options.get('zip_file')
        domain = options.get('domain')

        site = Site.objects.get(domain=domain)

        # self.stdout.writite(self.style.SUCCESS('Data loading'))

        data = get_parsed_data(json_file_path)

        unpack_media_files(path=zip_file_path)
        created_pages = load_page_tree(data=data, site=site)

        self.stdout.write(self.style.SUCCESS(
            'Data loading is completed. Created page url\n{page_url}'.format(
                page_url=get_page_urls(domain, created_pages)
            )
        ))
